package uk.co.c2b2.aac.bamboo;

import org.junit.Test;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class DummyAccountTest {

    @Test
    public void testCreation() {
        String[] holders = new String[1];
        holders[0] = "Some Holder";
        int sortCode = 325123;
        int acc = 45645645;
        LocalDateTime preConstruction = LocalDateTime.now();
        DummyAccount account = new DummyAccount(holders, acc, sortCode);
        LocalDateTime postConstruction = LocalDateTime.now();
        assertEquals(account.holderNames()[0], holders[0]);
        assertEquals(account.sortCode(), sortCode);
        assertEquals(account.accountNumber(), acc);
    }

    @Test
    public void testTransact() {
        String[] holders = new String[1];
        holders[0] = "Some Holder";
        int sortCode = 325123;
        int acc = 45645645;
        DummyAccount account = new DummyAccount(holders, acc, sortCode);
        double amount = 48.0;
        account.transact(amount);
        double balance = account.currentBalance();
        assertEquals(amount, balance, 0.05);
    }

    @Test
    public void testModificationOnBalance() {
        String[] holders = new String[1];
        holders[0] = "Some Holder";
        int sortCode = 325123;
        int acc = 45645645;
        DummyAccount account = new DummyAccount(holders, acc, sortCode);
        double amount = 45.23;
        account.transact(amount);
        double balance = account.currentBalance();
        balance += 34.13;
        double actualBalance = account.currentBalance();
        assertNotEquals(balance, actualBalance, 0.05);
    }

    @Test
    public void testSomethingElse() {
        String[] holders = new String[1];
        holders[0] = "Richmond Boateng";
        int sortCode = 329172;
        int accountNumber = 45544541;
        DummyAccount acct = new DummyAccount(holders, accountNumber, sortCode);
        assertEquals(acct.sortCode(), sortCode);
    }
}
