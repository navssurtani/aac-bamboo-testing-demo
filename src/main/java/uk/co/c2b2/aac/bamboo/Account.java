package uk.co.c2b2.aac.bamboo;

import java.time.LocalDateTime;

public interface Account {
    int accountNumber();
    int sortCode();
    String[] holderNames();
    double currentBalance();
    LocalDateTime dateOpened();
    void transact(double amount);
}
