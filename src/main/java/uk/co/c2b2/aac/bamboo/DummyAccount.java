package uk.co.c2b2.aac.bamboo;

import java.time.*;

public class DummyAccount implements Account {
    private final String[] holders;
    private final int accountNumber;
    private final int sortCode;
    private final ZonedDateTime accountOpened;
    private double balance;

    public DummyAccount(String[] holders, int accountNumber, int sortCode) {

        if (holders == null || holders.length == 0) {
            throw new IllegalArgumentException("Account holders cannot be empty or null");
        }
        for (String h : holders) {
            if (h == null) {
                throw new IllegalArgumentException("You cannot pass a null account holder");
            }
        }

        // Account number should be 8 digits between 40_000_000 and 50_000_000.
        if (accountNumber < 40_000_000 || accountNumber > 50_000_000) {
            throw new IllegalArgumentException("Invalid account number passed. Value should be between 40,000,000 and" +
                    " 50,000,000. Value passed: " + accountNumber);
        }

        if (sortCode < 300_000 || sortCode > 350_000) {
            throw new IllegalArgumentException("Invalid sort code passed. Value should be between 300,000 and 350,000" +
                    ". Value passed: " + sortCode);
        }

        this.holders = holders;
        this.accountNumber = accountNumber;
        this.sortCode = sortCode;
        this.accountOpened = ZonedDateTime.now(ZoneOffset.UTC);
        this.balance = 0.0;
    }

    public int accountNumber() {
        return this.accountNumber;
    }

    public int sortCode() {
        return this.sortCode;
    }

    public String[] holderNames() {
        return this.holders;
    }

    public final double currentBalance() {
        final Double copy = new Double(this.balance);
        return copy;
    }

    public LocalDateTime dateOpened() {
        return accountOpened.toLocalDateTime();
    }

    public void transact(double amount) {
        this.balance += amount;
    }
}
